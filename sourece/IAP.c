#include "HT45F67.h"
#include "IAP.h"
#include"typedef.h"
#include"uart.h"
volatile	unsigned char counti;
 volatile	unsigned char count;
const unsigned char buff_table[128]  __attribute__ ((at(0x7EC0))) ;

/*static volatile __16_type IAR_WR_Buff[64]  __attribute__((at(0x480)));*/
//volatile unsigned char WriteCount;
//volatile unsigned char WriteOffset;
//volatile unsigned char ReadOffset;
//volatile unsigned char ReadCount;
/********************************************************************
Function: Flash存儲器寫使能
INPUT	: None
OUTPUT	: None
NOTE	:
********************************************************************/
void fun_Enable_FWEN(void)
{
	unsigned char EMIStatus ;
	EMIStatus = _emi;
	_emi = 0;
	GCC_CLRWDT();
	_fc0 = ENABLE_FWEN_MODE; // 寫功能使能模式
	_fd1l = 0x00;			 // 按順序將固定模式數據序列寫入對應的暫存器
	_fd1h = 0x04;
	_fd2l = 0x0D;
	_fd2h = 0x09;
	_fd3l = 0xC3;
	_fd3h = 0x40;
	while(_fwpen)
	{
		GCC_CLRWDT();
	}
	_emi = EMIStatus;
}
///********************************************************************
//Function: 擦除Flash存儲器目標地址頁
//INPUT	: Flash_Addr
//OUTPUT	: None
//NOTE	:
//*********************************************************************/
void fun_Erase_Page(unsigned int EPage_AddrH,unsigned int EPage_Addrl)
{
	fun_Enable_FWEN();
	_farl = EPage_Addrl;
	_farh = EPage_AddrH;
	_fc0 = PAGE_ERASE_MODE;			//頁擦除模式
	_fwt = 1;
	while(_fwt)
	{
		GCC_CLRWDT();
	}
}
///********************************************************************
//Function: fun_IAPWrite_64Word
//INPUT	: Flash_Addr
//OUTPUT	: None
//NOTE	:
//*********************************************************************/
void fun_IAPWrite_64Word(unsigned int EPage_AddrH,unsigned int EPage_Addrl   )
{
	fun_Enable_FWEN();
	_fc0=WRITE_FLASH_MODE;

	_farl = EPage_Addrl;
	_farh = EPage_AddrH;	
volatile	unsigned char Data_Count=0;
	GCC_NOP();
	for(Data_Count=0;Data_Count<64;Data_Count++) //写byte//中間開寫的話會不寫到下一頁取
	{
		_fd0l = IAR_WR_Buff[Data_Count].byte.byte0;
		_fd0h = IAR_WR_Buff[Data_Count].byte.byte1;
	}
	_fwt = 1;
	while(_fwt)
		{
			GCC_CLRWDT();
		}
	_cfwen = 0;					//結束寫入，關閉寫使能
}

///********************************************************************
//Function: fun_IAPWrite_64Word
//INPUT	: Flash_Addr
//OUTPUT	: None
//NOTE	:
//*********************************************************************/
void fun_IAPReadData(unsigned int EPage_AddrH,unsigned int EPage_Addrl   )
{
// 	fun_Enable_FWEN();
	_fc0=READ_FLASH_MODE;
	_farl = EPage_Addrl;
	_farh = EPage_AddrH;
	GCC_NOP();
	unsigned char i;
	for (i = 0; i < 64; i++)
	{
   	_frd = 1;
   	while(_frd)
	{
		GCC_CLRWDT();
	}
	IAR_WR_Buff[i].byte.byte0 = _fd0l;
	IAR_WR_Buff[i].byte.byte1 = _fd0h;

   	 EPage_Addrl++;
   	_farl = EPage_Addrl;
   /*	if(EPage_Addrl>64)
   	 {
   		EPage_AddrH++;
   		_farh = EPage_AddrH;
   	 }
*/	}
	_frden=0;

}
///********************************************************************
//Function: fun_IAPWrite_64Word
//INPUT	: Flash_Addr
//OUTPUT	: None
//NOTE	:
//*********************************************************************/
void fun_IAPWdata( )
{
		unsigned char i;
	for (i = 0; i < 64; i++)
	{

	IAR_WR_Buff[i].byte.byte1 = i;
//	IAR_WR_Buff[i].byte.byte1 = i;
	}
	for (i = 0; i < 64; i++)
	{

	IAR_WR_Buff[i].byte.byte0 = 64-i;
//	IAR_WR_Buff[i].byte.byte1 = i;
	}
}
///********************************************************************
//Function: CLREA data
//INPUT	: Flash_Addr
//OUTPUT	: None
//NOTE	:
//*********************************************************************/
void fun_IAdataCreal_buff( )
{
		unsigned char i;
	for (i = 0; i < 64; i++)
	{

	IAR_WR_Buff[i].byte.byte0 = 0;
	IAR_WR_Buff[i].byte.byte1 = 0;
	}
}

void uart_txdata()
{
	
	if(gbv_UartTxing==0)
	{
		lu8v_TxBufoffset = 0;
		lu8v_TxBufoffset_count=0;
		gbv_UartTxing = 1;
		_acc = _usr;
		_txrrxr =IAR_WR_Buff[0].byte.byte0;
		uart_send_count++;
	}
}

//void HalFlashReadPage(unsigned int addr)
//{//一个Flash地址对应一个word （16位）
//	 unsigned char  i,j =0;
//	 unsigned int tmpAdr;
//	tmpAdr = addr&0xffc0;
//	_emi = 0;
//	_fc0=READ_FLASH_MODE; //select read flash rom mode
//	_frden=1; //Flash memory Read Enable Bit
//	for(i=0; i<64;i++)
//	{
//	_farl=( unsigned char)(tmpAdr&0xff); //write data to Flash address register
//	_farh=( unsigned char)((tmpAdr>>8)&0xff);//取地址首位，因为都是一个Page，所以地址首位是固定的
//	_frd=1; //activate a read cycle
//	while(_frd)
//	{
//	GCC_CLRWDT();
//	}
//	flash_RWBuff[j] = _fd0l;
//	j++;
//	flash_RWBuff[j] = _fd0h;
//	j++;
//	tmpAdr+=1;
//	}
//	_frden=0;
//	_emi = 1;
//}
//
///********************************************************************
//Function: CLREA data
//INPUT	: Flash_Addr
//OUTPUT	: None
//NOTE	:
//*********************************************************************/
void HalFlashReadBuff(volatile  uint16 addr,volatile uint8 *buf, uint8 len)
{
	volatile uint8 i,tmpFlag = 0;
	volatile uint16 tmpAdr;
	tmpAdr = addr;
	//HalFlashReadPage(addr);
	if(len%2==1)
	{
		len = len-1;
		tmpFlag = 1;
	}
	_emi = 0;
	
	_fc0=READ_FLASH_MODE; //select read flash rom mode
	_frden=1; //Flash memory Read Enable Bit
	for(i=0; i<len;i++)
	{
		_farl=(uint8)(tmpAdr&0xff); //write data to Flash address register
		_farh=(uint8)((tmpAdr>>8)&0xff);
		_frd=1; //activate a read cycle
	while(_frd)
	{
		GCC_CLRWDT();
	}
	*buf++ = _fd0l;
	*buf++ = _fd0h;
	//IAR_WR_Buff[i].byte.byte0 = _fd0l;
//	IAR_WR_Buff[i].byte.byte1 = _fd0h;

	tmpAdr++;
	i++;
	}
	if(tmpFlag == 1)
	{
		_farl=(uint8)(tmpAdr&0xff); //write data to Flash address register
		_farh=(uint8)((tmpAdr>>8)&0xff);
		_frd=1;
		while(_frd)
		{
		GCC_CLRWDT();
		}
		*buf++ = _fd0l;
	//	IAR_WR_Buff[i].byte.byte0 = _fd0l;

	}
	_frden=0;
	_emi = 1;
}

void BUff_to_IAR_WR_Buff()
{   count=0;unsigned char i;
	for ( i = 0; i < counti;  i++)
	{
	    if(i%2==0)
	    {
			IAR_WR_Buff[count].byte.byte0 = buff[ i];
	    }
	    else
	    {
	    	IAR_WR_Buff[count].byte.byte1 = buff[ i];
	    	count++;
	    }
	}

}

void BUffLClear()
{
 
	    unsigned char i;
	    for( i=0;i<128;i++)
	    {
	   	 buff[i]=0;
	    }
	    //	buff[i]=buff_table[i];

}
