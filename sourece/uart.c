//___________________________________________________________________
//___________________________________________________________________
//  Copyright : 2015 BY HOLTEK SEMICONDUCTOR INC
//  File Name : uart.c
// Description: 帶IR載波的UART傳輸
//   Customer : 健芳電子科技(上海)有限公司
//Targer Board: HT45F75 DEMO 開發板
//   MCU      : HT45F75
//___________________________________________________________________
//___________________________________________________________________
#include "HT45F67.h"
#include "IAP.h"
#include"typedef.h"
#include"uart.h"
//volatile unsigned char 	gu8v_UartTxBuf[14];
volatile unsigned char lu8v_TxBufoffset;
volatile unsigned char lu8v_TxBufoffset_count;
volatile unsigned char gbv_UartTxing;
/********************************************************************
Function: Uart初始化程序
INPUT	:
OUTPUT	:
NOTE	:
********************************************************************/
void fun_UartBaseInit()
{
// 數據傳輸格式設定
	_ucr1 = 0x80;
// 波特率設定
	_brgh = 1;
	_brg  = 25;
	_adden = 1;
	_wake = 0;
	_rie = 1;
	_tiie = 1;
	_teie = 1;
	_rxen = 1;
	_txen = 1;
	_urf  = 0;
	_txif = 0;
	_ure  = 1;
	_mf1e = 1;
	_emi  = 1;
	_pcfs0=1;

}

/******************************************************************
Function: Uart數據發送和接收中斷子程序
INPUT	:
OUTPUT	:
NOTE	:
********************************************************************/
DEFINE_ISR(UART_ISR, 0x014)
{
	if (_urf)// 復合中斷,首先判斷是否為UART中斷
	{
		_urf = 0;
		// 奇偶校验出错
		// if (_perr)
		// {
		// 	_acc = usr;
		// 	_acc = _txrrxr;
		// }
		// 噪声干扰错误
		if (_nf)
		{
			_acc = _usr;
			_acc = _txrrxr;
			//lu8v_RxBufoffset = 0;
		}
		// 帧错误
		if (_ferr)
		{
			_acc = _usr;
			_acc = _txrrxr;
		//	lu8v_RxBufoffset = 0;
		}
		// 溢出错误
		if (_oerr)
		{
			_acc = _usr;
			_acc = _txrrxr;
		//	lu8v_RxBufoffset = 0;
		}
		// 发送数据
		if (_txif)
		{
			if (lu8v_TxBufoffset < counti-1)
			{  
				if(lu8v_TxBufoffset%2==0)
				{
				_txrrxr = IAR_WR_Buff[lu8v_TxBufoffset_count].byte.byte1;
				lu8v_TxBufoffset_count++;
				}
				else
				{
				_txrrxr = IAR_WR_Buff[lu8v_TxBufoffset_count].byte.byte0;	
				}
	
			}
			else
			{
				gbv_UartTxing = 0;
			}
			lu8v_TxBufoffset++;
		}
		// 接收数据
		if (_rxif)
		{
          GCC_NOP();
		}
	}
}
//void uart_txdata()
//{
//	
//	if(gbv_UartTxing==0)
//	{
//	unsigned char offset;
//	for (offset = 0; offset < 14; offset++)
//	{
//		gu8v_UartTxBuf[offset] =offset ;
//	}
//	lu8v_TxBufoffset = 1;
//	gbv_UartTxing = 1;
//	_acc = _usr;
//	_txrrxr = gu8v_UartTxBuf[0];
//		
//	}
//}