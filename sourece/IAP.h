#ifndef _IAP_H
#define _IAP_H
#include"typedef.h"
#define uint16 unsigned int 
#define uint8  unsigned char  
#define MemoryStart_Addr (0x5B00) //


#define GLU_RecordInfo_Addr (0x5E80)
#define GLU_Code_Addr (0x5F00)  
#define GLU_Voice_Addr (0x5F40)  
#define DebugInfo_Addr (0x5F80)
#define GLU_DebugInfo_Addr (DebugInfo_Addr+0U)  //﹀縸?
#define AdjustOP_Addr (DebugInfo_Addr+8U)
#define PuductID_Addr        (0x5FC0)  

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 共用函數 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//static volatile __16_byte   IAR_WR_Buff[64]  __attribute__((at(0x480)));
//void fun_Erase_Page(unsigned int EPage_AddrH,unsigned int EPage_Addrl) ;
void fun_IAPWrite_64Word(unsigned int EPage_AddrH,unsigned int EPage_Addrl   );
void fun_Erase_Page( unsigned int  EPage_AddrH,unsigned int EPage_Addrl);
void fun_IAPReadData(unsigned int EPage_AddrH,unsigned int EPage_Addrl   ) ;
void HalFlashReadBuff(volatile  uint16 addr,volatile uint8 *buf, uint8 len);
void fun_IAdataCreal_buff( );
void fun_IAPWdata( );
void BUff_to_IAR_WR_Buff();
static volatile unsigned char buff[128]    __attribute__((at(0x380)));
 
//const unsigned char buff_table[128]  __attribute__ ((at(0x7EC0))) ;
static volatile __16_type IAR_WR_Buff[64]  __attribute__((at(0x480)));
void BUffLClear();
extern const unsigned char buff_table[128]  __attribute__ ((at(0x7EC0))) ;

extern volatile	unsigned char  uart_send_count;
extern volatile	unsigned char counti;
extern  volatile	unsigned char count;
//tern __16_byte   IAR_WR_Buff[64];
//#define EPage_AddrH 0x58
//#define EPage_Addrl 0x00
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 預定義 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//volatile __byte_type  static  IAR_WR_Buff[64]  __attribute__ ((at(0x480)));
#define 	PAGE_ERASE_MODE		0x90		// 塊擦除模式
#define 	READ_FLASH_MODE		0x32		// 讀Flash模式
#define 	ENABLE_FWEN_MODE	0x68 		// 寫功能使能模式
#define 	WRITE_FLASH_MODE	0x80		// 寫Flash模式

#endif